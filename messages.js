var msg     =   [
                    "This is a test",
                    "I am awesome",
                    "Nothing here, really",
                    "2 + 2 = 22",
                    location.hostname
                ],
    current,
    text    =   document.querySelector("input[type=text]"),
    speed   =   50,
    ms_min  =   4000,
    ms_max  =   6000;

text.value=msg[current=0];

function text_delete() {
    text.value=text.value.substr(0,text.value.length-1);
}

function text_add() {
    text.value+=msg[current][text.value.length];
}

setTimeout(function change() {
    var old=current;
    
    while (old===current) {
        current=~~((Math.random()*12345)%msg.length);
    }
    
    var len=text.value.length;
    var len2=msg[current].length;
    var i=0;
    
    while (i++,len--) {
        setTimeout(text_delete,speed*i);
    }
    while (i++,++len<len2) {
        setTimeout(text_add,speed*i);
    }
    
    setTimeout(change,(speed*(i+1)+ms_min)+Math.random()*(ms_max-ms_min));
},1000);
